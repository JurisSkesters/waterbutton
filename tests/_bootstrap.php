<?php
// This is global bootstrap for autoloading

//Init joomla
if (!defined('_JEXEC')) {
    define('_JEXEC', 1);
}
//Build Joomla path
$dir = dirname(__DIR__) . '/web';

if (file_exists($dir . '/defines.php')) {
    include_once $dir . '/defines.php';
}

if (!defined('_JDEFINES')) {
    define('JPATH_BASE', $dir);
    require_once JPATH_BASE . '/includes/defines.php';
}

define('_JDEFINES', 1);
define('_TESTING_MODE', 1);


require_once JPATH_BASE . '/includes/framework.php';

// Mark afterLoad in the profiler.
JDEBUG ? $_PROFILER->mark('afterLoad') : null;

// Instantiate the application.
$app = JFactory::getApplication('site');

//Add faker
// require the Faker autoloader
// alternatively, use another PSR-0 compliant autoloader (like the Symfony2 ClassLoader for instance)
require_once 'vendor/autoload.php';

//Add custom helpers
require_once 'library/autoload.php';
require_once 'web/libraries/custom/Api/Api.php';

