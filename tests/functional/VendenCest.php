<?php

class VendenCest
{

    public function _before(FunctionalTester $I)
    {
    }

    public function _after(FunctionalTester $I)
    {
    }

    // tests
    public function tryToOrderWater(FunctionalTester $I)
    {
        require_once(JPATH_ROOT . '/cli/worker.php');
        $I->wantTo('I want order water from Venden');
        //Create new Signal
        $signal = new \Api\Models\Signal();
        $signalSent = $signal->register(Uid);

        // Send to fake address
        $config = JFactory::getConfig();
        $config->set('vendenWaterOrderUrl', 'http://waterbutton.local/test-data/venden-response.html');

        $worker = new WorkerCron();
        $worker->orderVenden();
        //Check if application is ok
        $application = new \Api\Models\Application();
        $applications = $application->getNewApplications();
        $I->assertCount(1, $applications);
        $I->assertEquals($signalSent->id, $applications[0]->signal);
    }
}
