To setup xdebug with phpstorm enable profiler, then call Run->Start Listening for PHP Debug connections
You can define alias and run xon

Linux:
alias xon="export XDEBUG_CONFIG=\"profiler_enable=1\""
alias xoff="export XDEBUG_CONFIG=\"profiler_enable=0\""

Windows:
set XDEBUG_CONFIG=\"profiler_enable=1\"
set XDEBUG_CONFIG=\"profiler_enable=0\"

More info and credit:
http://theaveragedev.com/debug-cli-scripts-with-phpstorm/