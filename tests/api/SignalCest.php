<?php


class SignalCest
{

    private $apiBase = '/post/content/signals/';

    private $data = null;

    public function _before(ApiTester $I)
    {
    }

    public function _after(ApiTester $I)
    {
    }

    /**
     * @after tryToDelete
     */
    public function tryToRegister(ApiTester $I)
    {
        $I->wantTo('I want to register new signal');
        $I->amBearerAuthenticated(Token);
        $I->sendPost($this->apiBase);
        //Response code must be 200 and object must be returned
        $I->seeSoapResponseCodeIs('200');
        $result = json_decode($I->grabResponse());
        $this->data = $result->data;


        $dataPath = '$.data..';
        $I->seeResponseJsonMatchesJsonPath($dataPath . 'type');
        $I->seeResponseJsonMatchesJsonPath($dataPath . 'id');
        $I->assertEquals('signals', $result->data->type);
    }

    public function tryToDelete(ApiTester $I)
    {
        if (!$this->data) {
            $this->tryToRegister($I);
        }
        $I->wantTo('I want to delete new signal');
        $I->amBearerAuthenticated(Token);
        $I->sendDELETE('/delete/content/signals/' . $this->data->id);
        $I->seeSoapResponseCodeIs('204');
        $this->data = null;
    }

    public function tryUnauthorizedCall(ApiTester $I)
    {
        $I->wantTo('I want to access without valid token');
        $I->sendPost($this->apiBase);
        //Response code must be 200 and object must be returned
        $I->seeSoapResponseCodeIs('403');
    }


}
