<?php


namespace ApiTest;


class Helper
{


    public static function trunkTable($table)
    {
        $db = \JFactory::getDbo();
        $query = $db->getQuery(true);
        $query->delete($db->quoteName($table));
        $db->setQuery($query);
        $db->execute();
    }
}