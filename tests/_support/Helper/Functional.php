<?php
namespace Helper;

// here you can define custom actions
// all public methods declared in helper class will be available in $I

use Api\Models\Application;
use Api\Models\Signal;
use Codeception\TestInterface;
use \ApiTest\Model;

class Functional extends \Codeception\Module
{
    protected $insertedRows = [];

    public function cleanupDbTable()
    {
        $db = \JFactory::getDbo();

        //Get all ids
        foreach (array_reverse($this->insertedRows) as $row) {
            $query = $db->getQuery(true);
            $conditions = array(
                $db->quoteName('id') . ' = ' . $db->quote($row['primary'])
            );
            $table = str_replace('#__', '', $row['table']);
            $query->delete($db->quoteName('#__' . $table));
            $query->where($conditions);
            $db->setQuery($query);
            $db->execute();
        }
    }

    public function _after(TestInterface $test)
    {
        $this->cleanupDbTable();

        $signal = new Signal();
        $application = new Application();
        //Clean up tables tht cannot be cleaned using id method
        $trunkTables = array(
            $signal->table,
            $application->table
        );
        foreach ($trunkTables as $trunkTable) {
            self::trunkTable($trunkTable);
        }
    }

    public static function trunkTable($table)
    {
        $db = \JFactory::getDbo();
        $query = $db->getQuery(true);
        $query->delete($db->quoteName($table));
        $db->setQuery($query);
        $db->execute();
    }
}
