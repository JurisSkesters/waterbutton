<?php
/**
 * jBackend component for Joomla
 *
 * @author selfget.com (info@selfget.com)
 * @package jBackend
 * @copyright Copyright 2014 - 2015
 * @license GNU Public License
 * @link http://www.selfget.com
 * @version 2.1.3
 */

// no direct access
defined('_JEXEC') or die('Restricted access');
jimport('custom.ObjectAndXML');

require_once JPATH_COMPONENT . '/helpers/jbackend.php';
jimport('custom.logging.LoggerManager');

class jBackendViewRequest extends JViewLegacy
{
    protected $item;

    protected $params;

    protected $state;

    protected $logger;

    public function display($tpl = null)
    {
        // Initialise variables
        $app = JFactory::getApplication('site');
        $this->logger = LoggerManager::getLogger();


        $this->state = $this->get('State');
        $this->params = $this->state->get('parameters.menu');

        if (is_null($this->params)) {
            $params = JComponentHelper::getParams('com_jbackend');
            $access_type = $params->get('default_access_type', 'key'); // free/user/key
            $enable_trace = $params->get('default_enable_trace', '0');
            $enable_cors = '0'; // disabled
            $force_ssl = $params->get('default_force_ssl', '0');
            $enabled_modules = '1'; // all
            $selected_modules = array();
        } else {
            $access_type = $this->params->get('access_type', 'key'); // free/user/key
            $enable_trace = $this->params->get('enable_trace', '0');
            $enable_cors = $this->params->get('enable_cors', '0');
            $force_ssl = $this->params->get('force_ssl', '0');
            $enabled_modules = $this->params->get('enabled_modules', '1');
            $selected_modules = $this->params->get('selected_modules', array());
        }

        // Check if force to SSL
        if ($force_ssl != '0') {
            $uri = JUri::getInstance();
            if (strtolower($uri->getScheme()) != 'https') {
                // Forward to https
                $uri->setScheme('https');
                $app->redirect((string)$uri);
            }
        }

        $session = JFactory::getSession();
        $session->set('access_type', $access_type);
        $session->set('enabled_modules', $enabled_modules);
        $session->set('selected_modules', $selected_modules);

        if ($enable_trace) {
            // Collect pre-execution log information
            $log = array();
            $log['duration'] = -microtime(true);
            $log['request_time'] = gmdate("Y-m-d H:i:s", (int)(-$log['duration']));
            $log['request_date'] = gmdate("Y-m-d", (int)(-$log['duration']));
            $log['endpoint'] = $app->getMenu()->getActive()->id;
            switch ($access_type) {
                case 'user':
                    $log['access_type'] = 1;
                    break;
                case 'key':
                    $log['access_type'] = 2;
                    break;
                case 'free':
                default:
                    $log['access_type'] = 0;
                    break;
            }
        }

        //Try API
        try {
            $this->item = $this->get('Item');
            $response = $this->item;
        } catch (\Neomerx\JsonApi\Exceptions\JsonApiException $error) {

            $response = \Api\Api::getErrorResponse($error);

            //Add logger
            if ($response->errors[0]->status == "503") {
                $this->logger->fatal(json_encode($response));
            } else {
                $this->logger->error(json_encode($response));
            }
        }

        // Check for errors
        if (count($errors = $this->get('Errors'))) {
            JError::raiseError(500, implode("\n", $errors));
            return false;
        }

        if ($enable_trace) {
            // Collect post-execution log information
            $log['error'] = (int)($this->item['status'] == 'ko');
            $log['error_code'] = ($log['error']) ? $this->item['error_code'] : '';
            $log['user_id'] = JFactory::getUser()->id;
            $log['key'] = $app->input->getString('api_key', '');
            $log['module'] = $app->input->getString('module', '');
            $log['action'] = $app->input->getString('action', '');
            $log['resource'] = $app->input->getString('resource', '');
            $log['duration'] += microtime(true);

            // Save log information
            jBackendHelper::logRequest($log);
        }


        //Log all errors
        if (is_array($response) && $response['status'] == 'ko') {
            $this->logger->error("error_code:" . $this->item['error_code'] . ';error_description:' . $this->item['error_description']);
        }


        //Support for xml and json responses
        $format = $app->input->getString("format", "json");
        switch ($format) {
            case "json":
                // Use the correct json mime-type
                header('Content-Type: application/json');
                // Change the suggested filename
                header('Content-Disposition: attachment;filename="response.json"');
                // Output the JSON data
                echo json_encode($response);
                break;
            case "xml":
                // Use the correct json mime-type
                header('Content-Type: application/xml');
                // Change the suggested filename
                header('Content-Disposition: attachment;filename="response.xml"');
                // creating object of SimpleXMLElement
                $obj = new ObjectAndXML();
                $recordsXML = $obj->objToXML($response);
                echo $recordsXML;
                break;
            case "image":
                $fp = fopen($response, 'rb');
                $mimeType = image_type_to_mime_type(exif_imagetype($response));

                // send the right headers
                header("Content-Type: " . $mimeType);
                header("Content-Length: " . filesize($response));
                // dump the picture and stop the script
                fpassthru($fp);
                exit;
                break;
        }

        // Enable CORS
        if ($enable_cors != '0') {
            header('Access-Control-Allow-Origin: *');
        }
        JFactory::getApplication()->close();
    }


}
