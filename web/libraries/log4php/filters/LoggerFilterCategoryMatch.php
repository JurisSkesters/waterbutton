<?php
/**
 * This is a very simple filter based on category matching.
 */
class LoggerFilterCategoryMatch extends LoggerFilter {

	/**
	 * @var boolean
	 */
	protected $acceptOnMatch = true;

	/**
	 * @var string
	 */
	protected $stringToMatch;

	/**
	 * @param mixed $acceptOnMatch a boolean or a string ('true' or 'false')
	 */
	public function setAcceptOnMatch($acceptOnMatch) {
		$this->setBoolean('acceptOnMatch', $acceptOnMatch);
	}
	
	/**
	 * @param string $s the string to match
	 */
	public function setStringToMatch($string) {
		$this->setString('stringToMatch', $string);
	}

	/**
	 * @return integer a {@link LOGGER_FILTER_NEUTRAL} is there is no string match.
	 */
	public function decide(LoggerLoggingEvent $event) {
		$category = $event->getLoggerName();
		$colpos = strpos( $category, ':' );
		if( $colpos !== false )
			$category = substr( $category, 0, $colpos );
		
		if($this->stringToMatch === null) {
			return LoggerFilter::NEUTRAL;
		}
		
		if(trim($category) == trim($this->stringToMatch)) {
			return ($this->acceptOnMatch) ? LoggerFilter::ACCEPT : LoggerFilter::DENY;
		}
		return LoggerFilter::NEUTRAL;
	}
}
