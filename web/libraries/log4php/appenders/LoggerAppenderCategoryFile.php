<?php
/**
 * An Appender that picks logfile name out of the second part of the category.
 *
 * This appender uses a layout.
 * 
 * ##Configurable parameters:##
 * 
 * - **file** - Path to the target file. Should contain a %s which gets 
 *     substituted by the category part.
 * - **append** - If set to true, the appender will append to the file, 
 *     otherwise the file contents will be overwritten. Defaults to true.
 */
class LoggerAppenderCategoryFile extends LoggerAppenderFile
{
	/**
	 * Current category part which was used when opening a file.
	 * Used to determine if a rollover is needed when the category part changes.
	 * @var string
	 */
	protected $currentCP;

	/**
	 * Appends a logging event.
	 * 
	 * If the target file changes because of category part changes
	 * the current file is closed. A new file, with the new name, will be 
	 * opened by the write() method. 
	 */
	public function append( LoggerLoggingEvent $event )
	{
		$loggername = $event->getLoggerName();
		$colpos = strpos( $loggername, ':' );
		$CP = $colpos === false ? '' : substr( $loggername, $colpos + 1 );
		
		// Initial setting of current category part
		if (!isset($this->currentCP)) {
			$this->currentCP = $CP;
		} 
		
		// Check if rollover is needed
		else if ($this->currentCP !== $CP) {
			$this->currentCP = $CP;
			
			// Close the file if it's open.
			// Note: $this->close() is not called here because it would set
			//       $this->closed to true and the appender would not receive
			//       any more logging requests
			if (is_resource($this->fp)) {
				$this->write($this->layout->getFooter());
				fclose($this->fp);
			}
			$this->fp = null;
		}
	
		parent::append($event);
	}
	
	/**
	 * Determines target file. Replaces %s in file path with the category part. 
	 */
	protected function getTargetFile() {
		// sanitize category part to prevent malicious paths
		$this->currentCP = str_replace( array(':','.','/','\\'), '_', $this->currentCP );
		return str_replace('%s', $this->currentCP, $this->file);
	}
}
