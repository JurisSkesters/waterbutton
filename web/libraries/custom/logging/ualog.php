<?php

jimport('custom.logging.LoggerManager');

/**
 * User Activily log
 *
 * @author asalimbajevs
 */
class UALog {

    private static $instance = null;    
    protected $logger = null;

    private function __construct() {
        // private constructor
        $this->logger = LoggerManager::getLogger('MAINLOG');
    }

    /**
     * Returns user activity log object
     * 
     * @return  UALog object
     * 
     */
    public static function get() {
        if (self::$instance == null) {
            self::$instance = new UALog();
        }
        return self::$instance;
    }

    function userid($id = 0) {
        static $userid = 0;
        if ($id) {
            $userid = $id;
        }
        if (!$userid) {
            $userid = &JFactory::getUser()->id;
        }
        return $userid;
    }

    function predict_id($table) {
        $db = &JFactory::getDBO();

        $query = "SHOW TABLE STATUS LIKE '" . $db->replacePrefix($table) . "'";
        $db->setQuery($query);
        $result = $db->loadAssocList();

        $id = $result[0]['Auto_increment'];

        return $id;
    }

    function get_title($id, $table, $field = 'title', $key = 'id') {
        $db = &JFactory::getDBO();        

        $query = "SELECT $field FROM $table WHERE $key = $id";
        $db->setQuery($query);
        $title = $db->loadResult();

        return $title;
    }

    function get_titles($id, $table, $field = 'title') {
        $db = &JFactory::getDBO();
        $query = "SELECT GROUP_CONCAT( CONCAT('(',lang,') ',$field) SEPARATOR ' / ' ) AS x FROM $table WHERE id = $id";
        $title = $db->setQuery($query)->loadResult();
        return $title;
    }

    /**
     * Add record to UALog
     * 
     * @param type $alink link to item
     * @param type $atitle record description
     * @param type $item linked item
     * @param type $task controller task
     * @param type $com component name
     */
    function save($alink, $atitle, $item, $task=null, $com=null) {
        $db = &JFactory::getDBO();
        $time = time();        
        $com = $com ? $com : $db->Quote(JRequest::getVar('option'));
        $task = $task ? $task : $db->Quote(JRequest::getVar('task'));
        $user_id = $db->Quote($this->userid());
        $alink = $db->quote($alink);
        $atitle = $db->quote($atitle);
        $item = $db->quote($item);

        $query = "INSERT INTO #__ualog VALUES(NULL,$user_id,$com,$task,$alink,$atitle,$item,$time)";
        $db->setQuery($query);
        $db->query();

        if ($db->getErrorMsg()) {
            $this->logger->error("User activity log failed. Msg: {$atitle} {$item}. Error: ".$db->getErrorMsg());
        }
    }

}

?>