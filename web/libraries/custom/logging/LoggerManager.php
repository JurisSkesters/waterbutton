<?php
jimport('log4php.Logger');
/**
 * Helper class for log4php configuration and logger instace creation
 *
 * @author Jānis Upītis @ SIA "Datorikas institūts DIVI"
 * @author Askars Salimbajevs @ SIA "Datorikas institūts DIVI"
 */
class LoggerManager
{
    private static $configured = false;
    /**
     * Configures log4php, if necessary and returns logger of given name. If no name given, will return Root logger
     * @param <type> $name Name of logger
     * @return <type> Logger
     */
    public static function getLogger($name = null)
    {        
        if (! self::$configured)
        {
            $pathInfo = pathinfo(__FILE__);
            Logger::configure($pathInfo["dirname"].DIRECTORY_SEPARATOR.'configuration.xml');
            self::$configured = true;
        }
        if ($name != null)
        {
            return Logger::getLogger($name);
        }
        else return Logger::getRootLogger();
    }
}
?>