<?php

namespace Api\Schemas;

use \Neomerx\JsonApi\Schema\SchemaProvider;

/**
 * @package Neomerx\Samples\JsonApi
 */
class SignalSchema extends SchemaProvider
{
    protected $resourceType = 'signals';

    public function getId($item)
    {
        return $item->id;
    }

    public function getAttributes($item)
    {
        return ['created' => $item->created];
    }

}