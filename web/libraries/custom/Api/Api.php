<?php
namespace Api;

use Api\Models\HttpStatusCode;
use \Neomerx\JsonApi\Encoder\Encoder;
use \Neomerx\JsonApi\Encoder\EncoderOptions;
use Neomerx\JsonApi\Encoder\Parameters\EncodingParameters;
use Neomerx\JsonApi\Document\Error;
use Neomerx\JsonApi\Document\Link;
use Neomerx\JsonApi\Http\Request;
use \Neomerx\JsonApi\Factories\Factory;
use \Api\Schemas\AuthorSchema;
use \Api\Models\ErrorCode;
use Neomerx\JsonApi\Exceptions\JsonApiException;

jimport('custom.logging.LoggerManager');
require JPATH_LIBRARIES . '/custom/vendor/autoload.php';
require JPATH_LIBRARIES . '/custom/utility.php';
require_once 'autoload.php';

class Api
{
    public function __construct()
    {

    }

    public function getResponse($list)
    {
        //Return not found if list is empty
        if (!$list) {
            throw new JsonApiException(\Api\Models\Error::generateError(ErrorCode::RESOURCE_NOT_FOUND));
        }


        $parameters = self::getQueryParameters();

        $options = new EncodingParameters(
            $parameters->getIncludePaths(),
            $parameters->getFieldSets()
        );

        $encoder = Encoder::instance([
            Models\Signal::class => Schemas\SignalSchema::class
        ]);
        $result = $encoder->encodeData($list, $options);

        return json_decode($result);
    }

    public static function getErrorResponse($error)
    {
        $errors = $error->getErrors();
        $result = Encoder::instance()->encodeErrors($errors);
        http_response_code($errors[0]->getStatus());
        return json_decode($result);
    }

    public static function getQueryParameters()
    {
        $uri = \JUri::getInstance();
        $query = $uri->getQuery(true);

        $psr7request = new Request(
            function () use ($query) {
                return null;
            },
            function ($name) use ($query) {
                return null;
            }, function () use ($query) {
            return $query;
        });

        $factory = new Factory();
        $parameters = $factory->createQueryParametersParser()->parse($psr7request);
        return $parameters;
    }

    public static function getEmptyResponse()
    {
        http_response_code(HttpStatusCode::HTTP_NO_CONTENT);
        return "";
    }

    public static function getNullResponse()
    {
        http_response_code(HttpStatusCode::HTTP_OK);
        $a = new \stdClass();
        $a->data = null;
        return $a;
    }
}