<?php


namespace Api;

use Neomerx\JsonApi\Exceptions\JsonApiException;
use \Api\Models\ErrorCode;
use \Api\Models\Error;


class Helper
{
    /**
     * Takes an array single value list and transforms to "some","some2" string for using mainly in db queries
     * @param $array
     * @return mixed
     */
    public static function arrayToQuotedCsv($array)
    {
        return implode(",",
            array_map(function ($string) {
                return \JFactory::getDbo()->quote($string);
            },
                $array)
        );
    }

    public static function getPagination()
    {
        //Add pagination
        $queryParameters = Api::getQueryParameters();
        $pagination = $queryParameters->getPaginationParameters();
        $config = \JFactory::getConfig();

        //Set default limit
        //Cast size and number to positive int for minor security
        $pageSize = abs($pagination['size']) ?: $config->get("DEFAULT_PAGE_ITEM_LIMIT");
        $pageNumber = abs($pagination['number']) ?: 1;
        $offset = ($pageNumber - 1) * $pageSize;
        $pageSeed = $pagination['seed'];

        $page = new \stdClass();
        $page->size = $pageSize;
        $page->number = $pageNumber;
        $page->seed = $pageSeed;
        $page->offset = $offset;

        return $page;
    }

    public static function curl($url, $params = null)
    {
        $connectionTimeout = isset($params['CURLOPT_CONNECTTIMEOUT_MS']) ?: 4000;
        $curlTimeout = isset($params['CURLOPT_TIMEOUT_MS']) ?: 30000;
        $encoding = isset($params['CURLOPT_ENCODING']) ?: 'UTF-8';

        //Get remote file
        try {
            $curl = curl_init();

            curl_setopt_array($curl, Array(
                CURLOPT_USERAGENT => 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.2 (KHTML, like Gecko) Chrome/22.0.1216.0 Safari/537.2',
                CURLOPT_URL => $url,
                CURLOPT_RETURNTRANSFER => TRUE,
                CURLOPT_SSL_VERIFYPEER => false, //Skip ssl verification
                CURLOPT_ENCODING => $encoding,
                CURLOPT_HEADER => FALSE,
                CURLOPT_NOSIGNAL => 1,
                CURLOPT_CONNECTTIMEOUT_MS => $connectionTimeout, //The number of miliseconds to wait while trying to connect. Use 0 to wait indefinitely.
                CURLOPT_TIMEOUT_MS => $curlTimeout //The maximum number of miliseconds to allow cURL functions to execute.
            ));

            //Post
            if ($params['CURLOPT_POSTFIELDS']) {
                curl_setopt($curl, CURLOPT_POSTFIELDS,
                    http_build_query($params['CURLOPT_POSTFIELDS']));
            }

            if ($params['CURLOPT_HTTPHEADER']) {
                curl_setopt($curl, CURLOPT_HTTPHEADER, $params['CURLOPT_HTTPHEADER']);
            }


            //Set proxy
            \Utility::setCurlProxy($curl);
            $response = curl_exec($curl);

            if (!$response) {
                \LoggerManager::getLogger()->fatal('Curl ' . $url . ' failed' . curl_error($curl));
                throw new JsonApiException(\Api\Models\Error::generateError(ErrorCode::CURL_FAILED, array('details' => 'Empty response from url ' . $url . ' Curl error: ' . curl_error($curl))));
            }
        } catch (Exception $e) {
            \LoggerManager::getLogger()->fatal('Curl ' . $url . ' failed' . curl_error($curl));
            throw new JsonApiException(\Api\Models\Error::generateError(ErrorCode::CURL_FAILED, array('details' => 'Curl failed ' . $url . ' with exception: ' . curl_error($curl))));
        } finally {
            curl_close($curl);
        }

        return $response;
    }
}