<?php
namespace Api\Models;

class Error extends \Api\Models\Base
{
    public static function generateError($code, $params = null)
    {
        switch ($code) {
            case ErrorCode::NOT_IMPLEMENTED:
                $error = self::shortError(
                    HttpStatusCode::HTTP_SERVICE_UNAVAILABLE,
                    ErrorCode::NOT_IMPLEMENTED,
                    'Request not implemented'
                );
                break;
            case ErrorCode::SYSTEM_FAILURE:
                $error = self::shortError(
                    HttpStatusCode::HTTP_INTERNAL_SERVER_ERROR,
                    ErrorCode::SYSTEM_FAILURE,
                    'Internal system error',
                    $params['details']
                );
                break;
            case ErrorCode::TOKEN_FAILED:
                $error = self::shortError(
                    HttpStatusCode::HTTP_FORBIDDEN,
                    ErrorCode::TOKEN_FAILED,
                    'Authorization failed',
                    $params['details']
                );
                break;
            case ErrorCode::CURL_FAILED:
                $error = self::shortError(
                    HttpStatusCode::HTTP_SERVICE_UNAVAILABLE,
                    ErrorCode::CURL_FAILED,
                    'Failed to retrieve data from url',
                    $params['details']
                );
                break;

        }
        return $error;
    }

    private static function shortError($status, $code, $title, $detail = null)
    {
        return new \Neomerx\JsonApi\Document\Error(
            null,
            null,
            $status,
            $code,
            $title,
            $detail
        );
    }
}

?>