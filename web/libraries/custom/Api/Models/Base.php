<?php
namespace Api\Models;


class Base extends \stdClass
{

    public function instance($properties = array(), $class)
    {
        $item = new $class();

        foreach ($properties as $key => $value) {
            $item->{$key} = $value;
        }
        //If method available perform transformation
        if (method_exists($class, 'transform')) {
            $item::transform($item);
        }
        return $item;
    }


}

?>