<?php

namespace Api\Models;

use Api\Api;


class Application extends \stdClass
{
    public $table = '#__application';

    public function registerApplication($signal): \stdClass
    {
        $db = \JFactory::getDbo();
        $item = new \stdClass();
        $item->signal = $signal;

        try {
            $db->insertObject($this->table, $item, 'id');
            return $this->getItem($item->id);
        } catch (\Throwable $e) {
            \LoggerManager::getLogger()->fatal('Error inserting record:' . $e);
            throw new \Exception($e);
        }
    }

    public function queryBase()
    {
        $db = \JFactory::getDbo();
        $query = $db->getQuery(true);
        $query
            ->select('*')
            ->from($db->quoteName($this->table, 'apl'));

        return $query;
    }

    public function markAsProcessed($item)
    {
        $item->state = State::Processed;
        \JFactory::getDbo()->updateObject($this->table, $item, 'id');
    }

    public function getItem($id): \stdClass
    {
        $db = \JFactory::getDbo();
        $query = $this->queryBase();
        $query->where($db->quoteName('id') . '=' . $db->quote($id));
        $db->setQuery($query);
        return $db->loadObject();
    }

    public function getNewApplications(): array
    {
        return $this->getApplications(array('state' => State::New));
    }

    public function getProcessedApplications(): array
    {
        return $this->getApplications(array('state' => State::Processed));
    }

    public function getApplications($params = null): ?array
    {
        $db = \JFactory::getDbo();
        $query = $db->getQuery(true);
        $query
            ->select('*')
            ->from($db->quoteName($this->table, 'a'));

        if ($params['state']) {
            $query->where($db->quoteName('state') . '=' . $params['state']);
        }

        $db->setQuery($query);
        return $db->loadObjectList();
    }


}

abstract class State
{
    const New = 0;
    const Processed = 1;
    // etc.
}
