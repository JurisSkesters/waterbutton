<?php
/**
 * Created by PhpStorm.
 * User: jskesters
 * Date: 17.01.2017.
 * Time: 7:29
 */

namespace Api\Models;


class Venden
{
    public function order()
    {
        $todayApplication = $this->getApplication();
        $todaySignal = $this->getSignal();
        if ($todayApplication || !$todaySignal) {
            return;
        } elseif ($todaySignal) {
            //Log request
            $application = new Application();
            $item = $application->registerApplication($todaySignal->id);
            //Send request
            $result = $this->sendRequest();
            //Log response
            if ($result) {
                $application->markAsProcessed($item);
                $this->sendNotification();
            }
        }
    }

    private function sendNotification()
    {
        $this->sendSlackMessage();
    }

    private function sendSlackMessage()
    {
        $message = 'Pasūtīju ūdeni!';
        $config = new \JConfig();
        $client = new \Maknz\Slack\Client($config->slackWebHookUrl,
            [
                'username' => 'Water Boy',
                'channel' => $config->slackChannel,
                'link_names' => true
            ]
        );
        $client->send($message);
    }

    public function getSignal()
    {
        $db = \JFactory::getDbo();
        $date = new \JDate();
        $today = $date->format('Y-m-d');
        $signal = new Signal();
        $query = $signal->queryBase();
        //Get only signals created today
        $query->where($db->quoteName('created') . ' BETWEEN ' . $db->quote($today . ' 00:00:00') . ' AND ' . $db->quote($today . ' 23:59:59'));
        $query->setLimit(1);
        $db->setQuery($query);
        $item = $db->loadObject();
        return $item;
    }

    public function getApplication()
    {
        $db = \JFactory::getDbo();
        $date = new \JDate();
        $today = $date->format('Y-m-d');
        $application = new Application();
        $query = $application->queryBase();
        //Get only applications created today
        $query->where($db->quoteName('created') . ' BETWEEN ' . $db->quote($today . ' 00:00:00') . ' AND ' . $db->quote($today . ' 23:59:59'));
        $query->setLimit(1);
        $db->setQuery($query);
        $item = $db->loadObject();
        return $item;
    }

    private function sendRequest(): bool
    {
        $conf = \JFactory::getConfig();
        $url = $conf->get('vendenWaterOrderUrl');
        $postFields = $this->prepareRequest();
        $response = \Api\Helper::curl($url, array(
            'CURLOPT_POSTFIELDS' => $postFields,
            'CURLOPT_HTTPHEADER' => array('Content-Type: application/x-www-form-urlencoded; charset=UTF-8'
            )));

        if ($this->responseOk($response)) {
            return true;
        } else {
            \LoggerManager::getLogger()->fatal('Response failed ' . $response);
            return false;
        }
    }

    private function prepareRequest(): array
    {
        $config = \JFactory::getConfig();
        return array(
            'Contract_No_' => '0103952347',
            'Requested_Delivery_Date' => $this->calculateDeliveryDate()->format('Y-m-d', true), // 2017-06-03
            'Requested_Delivery_Time' => 2,
            'Ship_to_City' => 'Rīga',
            'Ship_to_Street' => 'Alfrēda Kalniņa iela',
            'Ship_to_House_No_' => '2-7',
            'Company_Name' => 'DIVI GRUPA SIA',
            'Requester_Name' => $config->get('requesterName'),
            'Requester_Phone' => $config->get('requesterPhone'),
            'Requester_E_Mail' => $config->get('requesterEmail'),
            'Long_Comments' => '5. stāvs',
            'Bottle_Count_18_9_L' => 2,
            'Bottle_Count_11_L' => '',
            'Returned_Bottle_Count_18_9_L' => 2,
            'Returned_Bottle_Count_11_L' => ''
        );
    }

    private function responseOk(string $response): bool
    {
        $expectedOkResponse = '/lv/pasutijums-neautorizets/?apstiprinats';
        $actualResponse = (json_decode($response))->continue;
        $result = ($actualResponse === $expectedOkResponse);
        return $result;
    }

    private function calculateDeliveryDate(): \JDate
    {
        date_default_timezone_set('Europe/Riga');
        $deliveryDate = new \JDate('now', new \DateTimeZone('Europe/Riga'));
        $now = $deliveryDate;

        if ($now->format('H:i', true) >= '00:00' && $now->format('H:i', true) <= '18:00') {
            $deliveryDate = $deliveryDate->add(new \DateInterval('P1D'));
        } else {
            $deliveryDate = $deliveryDate->add(new \DateInterval('P2D'));
        }
        //Weekend
        if ($deliveryDate->format('N', true) == 6) {
            $deliveryDate = $deliveryDate->add(new \DateInterval('P2D'));
        } elseif ($deliveryDate->format('N', true) == 7) {
            $deliveryDate = $deliveryDate->add(new \DateInterval('P1D'));
        }

        return $deliveryDate;
    }

}