<?php

namespace Api\Models;

use Api\Api;


class Device extends \stdClass
{
    public static function generateUid(): string
    {
        $uid = uniqid('', true);
        return $uid;
    }

    public static function generateToken(): string
    {
        $jwt = new \JJwt();
        $uid = self::generateUid();
        $token = $jwt->getToken(array("uid" => $uid));
        return $token;
    }
}


