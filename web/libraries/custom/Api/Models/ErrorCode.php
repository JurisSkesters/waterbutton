<?php
namespace Api\Models;

class ErrorCode extends \Api\Models\Base
{
    const NOT_IMPLEMENTED = "NOT_IMPLEMENTED";
    const SYSTEM_FAILURE="SYSTEM_FAILURE";
    const TOKEN_FAILED = "TOKEN_FAILED";
    const CURL_FAILED = "CURL_FAILED";
}

?>