<?php

namespace Api\Models;

use Api\Api;

class Signal extends \stdClass
{
    public $table = '#__signal';

    public function registerRequest(): Signal
    {
        $app = \JFactory::getApplication();
        $tokenData = json_decode($app->input->getString('tokendata'));
        return $this->register($tokenData->uid);
    }

    public function register($uid): Signal
    {
        //TODO: validate device id
        $db = \JFactory::getDbo();
        $item = new \stdClass();
        $item->device = $uid;

        try {
            $db->insertObject($this->table, $item, 'id');
            return $this->getItem($item->id);
        } catch (\Exception $e) {
            \LoggerManager::getLogger()->fatal('Error inserting record:' . $e);
            throw new JsonApiException(\Api\Models\Error::generateError(ErrorCode::SYSTEM_FAILURE));
        }
    }

    public function deleteRequest(): string
    {
        $app = \JFactory::getApplication();
        $this->delete($app->input->getString('id'));
        return Api::getEmptyResponse();
    }

    public function delete($id): void
    {
        $db = \JFactory::getDbo();
        $query = $db->getQuery(true);

        $conditions = array(
            $db->quoteName('id') . ' = ' . $db->quote($id)
        );

        $query->delete($db->quoteName($this->table));
        $query->where($conditions);
        $db->setQuery($query);
        $db->execute();
    }

    public function getItem($id): Signal
    {
        $db = \JFactory::getDbo();
        $query = $this->queryBase();
        $query->where($db->quoteName('id') . '=' . $db->quote($id));
        $db->setQuery($query);
        return $db->loadObject(Signal::class);
    }

    public function queryBase()
    {
        $db = \JFactory::getDbo();
        $query = $db->getQuery(true);
        $query
            ->select('*')
            ->from($db->quoteName($this->table, 'sig'));

        return $query;
    }


}
