<?php defined('_JEXEC') or die;

function util_handle_sync_failure($where, $error)
{
    $where = JText::_($where);
    $error = JText::_($error);
    $auto = php_sapi_name() == 'cli';
    $user = $auto ? '-' : JFactory::getUser()->name;
    $time = JFactory::getDate()->format("H:i");
    $rep = array('%WHERE%' => $where, '%ERROR%' => $error, '%USER%' => $user, '%TIME%' => $time);

    $config = JFactory::getConfig();

    $mailto = $config->get('SYNC_FAIL_MAIL_TO');
    $mailsub = $config->get('SYNC_FAIL_MAIL_SUBJECT');
    $mailbody = $config->get('SYNC_FAIL_MAIL_BODY');

    $recipients = explode(',', $mailto);
    foreach ($recipients as &$rcpt1)
        $rcpt1 = trim($rcpt1);
    $recipients = array_filter($recipients);
    unset($rcpt1);
    if (!count($recipients))
        return;

    $mailer = JFactory::getMailer();
    $mailer->setSender(array($config->get('mailfrom'), $config->get('fromname')));
    $mailer->addRecipient($recipients);
    $mailer->Encoding = 'base64';
    $mailer->setSubject(strtr($mailsub, $rep));
    $mailer->setBody(strtr($mailbody, $rep));
    $send = $mailer->Send();
    /*
        TODO: log sending failures
    */
    if ($send !== true) {
        $err = error_get_last();
        echo "$send ($err[message])";
        die;
    }
}

class Utility
{

    /***
     * Set offset
     * @param $offset - format ="+08:00"
     */
    static function setTimezone($offset)
    {
        $db = JFactory::getDbo();
        $db->setQuery('SET time_zone = "' . $offset . '";');
        $db->execute();
    }

    /***
     * Convert Europe/Riga to UTC
     * @param $datetime
     * @param string $format
     * @return string
     */
    static function toUtc($datetime, $format = "Y-m-d H:i:s")
    {
        $src_tz = new DateTimeZone('Europe/Riga');
        $dest_tz = new DateTimeZone('UTC');

        $dt = new DateTime($datetime, $src_tz);
        $dt->setTimeZone($dest_tz);

        return $dt->format($format);
    }

    /***
     * @param $curl
     */
    static function setCurlProxy(&$curl)
    {

        //Get values from config
        $config = JFactory::getConfig();
        $proxy = $config->get('PROXY');
        $proxyauth = $config->get('PROXYUSERPWD');

        //Set proxy
        if ($proxy) {
            curl_setopt($curl, CURLOPT_PROXY, $proxy);
            if ($proxyauth) {
                curl_setopt($curl, CURLOPT_PROXYUSERPWD, $proxyauth);
            }
        }
    }

    /****
     * Returns users IP
     * @return string
     */
    public static function getUserIP()
    {
        if (isset($_SERVER)) {
            if (isset($_SERVER["HTTP_X_FORWARDED_FOR"])) {
                $ip = $_SERVER["HTTP_X_FORWARDED_FOR"];
                if ($ip != '' && strtolower($ip) != 'unknown') {
                    $addresses = explode(',', $ip);
                    return $addresses[count($addresses) - 1];
                }
            }

            if (isset($_SERVER["HTTP_CLIENT_IP"]) && $_SERVER["HTTP_CLIENT_IP"] != '')
                return $_SERVER["HTTP_CLIENT_IP"];

            return $_SERVER["REMOTE_ADDR"];
        }

        if ($ip = getenv('HTTP_X_FORWARDED_FOR')) {
            if (strtolower($ip) != 'unknown') {
                $addresses = explode(',', $ip);
                return $addresses[count($addresses) - 1];
            }
        }

        if ($ip = getenv('HTTP_CLIENT_IP')) {
            return $ip;
        }

        return getenv('REMOTE_ADDR');
    }
}

 

