<?php
//Restrict access
defined('JPATH_PLATFORM') or die;
//Include library https://github.com/firebase/php-jwt/
require_once('JWT.php');
jimport('custom.logging.LoggerManager');

use \Firebase\JWT\JWT;

class JJwt
{
    private $secret; //Can be used for symmetric HS256
    private $token;
    private $logger;

    /***
     * @param null $params
     */
    function __construct($params = null)
    {
        $config = JFactory::getConfig();
        $this->logger = LoggerManager::getLogger('OAUTH');
        $this->secret = $config->get("JWT_SECRET");
        $this->algorithm = "HS256";
        $exp = 365 * 100; //100 Years
        $this->token = array(
            "iat" => time() - (60), //UNIX timestamp - 60 secs
            "exp" => time() + ($exp * 24 * 60 * 60)//x days; 24 hours; 60 mins; 60 secs
        );

    }

    /***
     * @param null $params
     * @return string
     */
    public function getToken($params = null)
    {
        //Add extra fields to token
        foreach ($params as $key => $value) {
            $this->token[$key] = $value;
        }
        //Try to create token
        try {
            $jwt = JWT::encode($this->token, $this->secret, $this->algorithm);
            return $jwt;
        } catch (Exception $e) {
            $this->logger->fatal('Token generation fail: ' . $e->getMessage(), $e);
            return $e->getMessage();

        }
    }

    /***
     * @param $token
     * @return bool|string
     */
    public function checkToken($token)
    {
        try {
            JWT::decode($token, $this->secret, array($this->algorithm));
            return true;
        } catch (Exception $e) {
            $this->logger->warn('Token check fail: ' . $e->getMessage(), $e);
            return $e->getMessage();
        }
    }

    public function decode($token)
    {
        return JWT::decode($token, $this->secret, array($this->algorithm));
    }

    public function authenticate()
    {
        //Get auth header
        $headers = $this->detectHeaders();
        //Check auth type
        if ($headers['Authorization']) {
            $data = explode(' ', $headers['Authorization']);
            $type = $data[0];
            if ($type === "Bearer") {
                $token = $data[1];
                $result = $this->checkToken($token);
                if ($result === true) {
                    $tokendata = $this->decode($token);
                    //Check if user id is present
                    if (!$tokendata->uid) {
                        $message = "Device not found";
                        $this->logger->error('Invalid authorization: ' . $message);
                        return $message;
                    } else {
                        //Set token data as param
                        $app = JFactory::getApplication();
                        $app->input->set("tokendata", json_encode($tokendata));
                    }
                }
                return $result;
            } else {
                $message = "Authorization method not found";
                $this->logger->error('Invalid authorization: ' . $message);
                return $message;
            }

        } else {
            $message = "Authorization header not found";
            $this->logger->error('Invalid authorization: ' . $message);
            return $message;
        }
    }

    public function setTokenData()
    {
        //Get auth header
        $headers = $this->detectHeaders();
        if (isset($headers['Authorization'])) {
            $data = explode(' ', $headers['Authorization']);
            $type = $data[0];
            if ($type === "Bearer") {
                $token = $data[1];
                $result = $this->checkToken($token);
                if ($result === true) {
                    $tokendata = $this->decode($token);
                    //Set token data as param
                    $app = JFactory::getApplication();
                    $app->input->set("tokendata", json_encode($tokendata));
                }
            }
        }
    }

    /***
     * @return array|false|null
     */
    protected function detectHeaders()
    {
        $headers = null;
        if (function_exists('getallheaders')) // If php is working under Apache, there is a special function
        {
            $headers = getallheaders();
        } else // Else we fill headers from $_SERVER variable
        {
            $headers = array();

            foreach ($_SERVER as $name => $value) {
                if (substr($name, 0, 5) == 'HTTP_') {
                    $headers[str_replace(' ', '-', ucwords(strtolower(str_replace('_', ' ', substr($name, 5)))))] = $value;
                }
            }
        }
        return $headers;

    }


}

