<?php
/**
 * jBackend user plugin for Joomla
 *
 * @author selfget.com (info@selfget.com)
 * @package jBackend
 * @copyright Copyright 2014 - 2015
 * @license GNU Public License
 * @link http://www.selfget.com
 * @version 2.1.3
 */

// no direct access
defined('_JEXEC') or die('Restricted access');
jimport('custom.logging.LoggerManager');
jimport('custom.jwt.jjwt');
jimport('custom.jbackend.jbackend');
jimport('custom.Api.Api');


class plgJBackendContent extends JPlugin
{
    public function __construct(& $subject = null, $config = null)
    {
        if ($subject && $config) {
            parent::__construct($subject, $config);
        }

        $this->api = new \Api\Api();
    }

    public function registerApplication(&$response)
    {
        $response = \Api\Models\Application::registerApplication();
    }

    public function registerSignal(&$response): void
    {
        $signal = new \Api\Models\Signal();
        $item = $signal->registerRequest();
        $response = $this->api->getResponse($item);
    }

    public function deleteSignal(&$response): void
    {
        $signal = new \Api\Models\Signal();
        $response = $signal->deleteRequest();
    }


    /**
     * Fulfills requests for event module
     *
     * @param   object $module The module invoked
     * @param   object $response The response generated
     * @param   object $status The boundary conditions (e.g. authentication status)
     *
     * @return  boolean   true if there are no problems (status = ok), false in case of errors (status = ko)
     */
    public function onRequestContent($module, &$response, &$status = null)
    {
        if ($module !== 'content') return true;

        // Add to module call stack

        jBackendHelper::moduleStack($status, 'content');

        $app = JFactory::getApplication();
        $input = $app->input;
        $action = $input->getString('action');

        if (is_null($action)) {
            $response = plgJBackendEvent::generateError('REQ_ANS'); // Action not specified
            return false;
        }

        $resource = $input->getString('resource');

        switch ($resource) {
            case 'applications':
                if ($action === "post") {
                    return $this->registerApplication($response);
                    break;
                }
            case 'signals':
                if ($action === "post") {
                    return $this->registerSignal($response);
                    break;
                } elseif ($action === "delete") {
                    return $this->deleteSignal($response);
                    break;
                }

        }
        return true;


    }
}







